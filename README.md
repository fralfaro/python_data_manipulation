# Introducción a la manipulación de datos
<a href="https://fralfaro.gitlab.io/python_data_manipulation/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/docs-link-brightgreen"></a>
[![pipeline status](https://gitlab.com/FAAM/python_data_manipulation/badges/master/pipeline.svg)](https://gitlab.com/FAAM/python_data_manipulation/-/commits/master)

## Contenidos temáticos

- Computación Cientifica
- Manipulación de datos
- Visualización de datos

